//
//  GuessTheFlagView.swift
//  GuessTheFlag
//
//  Created by Karla Pangilinan on 6/3/24.
//

import SwiftUI

struct GuessTheFlagView: View {
  @State private var shouldReset = false
  @State private var showingScore = false
  @State private var scoreTitle = ""
  @State private var alertMessage = ""
  @State private var currentScore = 0
  @State private var questionTotal = 0

  @State private var countries = ["UK", "US", "Spain", "Russia",
                                  "Poland", "Nigeria", "Monaco", "Italy",
                                  "Ireland", "Germany", "France", "Estonia"].shuffled()
  @State private var correctAnswer = Int.random(in: 0..<3)

  private let numberOfQuestions = 10

  var body: some View {
    ZStack {
      RadialGradient(stops: [
        .init(color: Color(red: 0.2, green: 0.1, blue: 0.2), location: 0.3), // 30%
        .init(color: Color(red: 0.55, green: 0.1, blue: 0.2), location: 0.3)
      ], center: .top, startRadius: 200, endRadius: 700)
      .ignoresSafeArea()

      VStack {
        Spacer()
        Text("Guess The Flag")
          .largeBlueFontStyle()
        Spacer()
        HStack {
          Text("Question #\(questionTotal + 1)")
            .foregroundColor(.white)
            .font(.title2.weight(.medium))
          Spacer()
          Text("Score: \(currentScore) / \(questionTotal)")
            .foregroundColor(.white)
            .font(.title2.weight(.medium))
        }
        .padding()
        VStack(spacing: 15) {
          VStack {
            Text("Tap the flag of")
              .foregroundStyle(.secondary)
              .font(.subheadline.weight(.heavy))
            Text(countries[correctAnswer])
              .font(.largeTitle.weight(.semibold))
          }
          ForEach(0..<3) { number in
            Button {
              flagTapped(number)
            } label: {
              FlagImage(country: countries[number])
            }
            .alert(scoreTitle, isPresented: $showingScore) {
              Button("OK", action: refreshQuestion)
            } message: {
              Text("Your answer is \(scoreTitle)\(alertMessage)")
            }
            .alert("Final Score", isPresented: $shouldReset) {
              Button("OK", action: resetScore)
            } message: {
              Text("\(currentScore) / \(questionTotal)")
            }
          }
        }
        .frame(maxWidth: .infinity)
        .padding(20)
        .background(.thinMaterial)
        .clipShape(RoundedRectangle(cornerRadius: 20))
        Spacer()
        Spacer()
      }
      .padding()
    }
  }

  private func flagTapped(_ number: Int) {
    questionTotal += 1
    if number == correctAnswer {
      scoreTitle = "Correct!"
      alertMessage = ""
      currentScore += 1
    } else {
      scoreTitle = "Wrong :("
      alertMessage = " That's the flag of \(countries[number])."
    }

    if questionTotal == numberOfQuestions {
      shouldReset = true
    } else {
      showingScore = true
    }
  }

  private func refreshQuestion() {
    countries.shuffle()
    correctAnswer = Int.random(in: 0..<3)
  }

  private func resetScore() {
    currentScore = 0
    questionTotal = 0
  }
}


#Preview {
    GuessTheFlagView()
}
