//
//  GridStack.swift
//  GuessTheFlag
//
//  Created by Karla Pangilinan on 6/5/24.
//

import SwiftUI

struct GridStack<Element: View>: View {
  let rows: Int
  let columns: Int
  let content: (Int, Int) -> Element // ready function to execute

  /*
   SwiftUI has to know how to identify every item in this uniquely
   */
  var body: some View {
    VStack {
      ForEach(0..<rows, id: \.self) { i in
        HStack {
          ForEach(0..<columns, id: \.self) { j in
            content(i, j) // execute the function that returns an Element (View)
          }
        }
      }
    }
  }
}
