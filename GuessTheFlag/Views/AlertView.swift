//
//  AlertView.swift
//  GuessTheFlag
//
//  Created by Karla Pangilinan on 6/3/24.
//

import SwiftUI

struct AlertView: View {
  @State private var showingAlert = false

  var body: some View {
    Button("Show Alert") {
      showingAlert = true
    }
    .padding()
    .alert("Important Message", isPresented: $showingAlert) {
      Button("Delete in Red", role: .destructive) { }
      Button("Cancel in Blue", role: .cancel) { }
      Button("OK in def") { }
    } message: {
      Text("This is the description")
    }
  }
}


#Preview {
    AlertView()
}
