//
//  FlagImage.swift
//  GuessTheFlag
//
//  Created by Karla Pangilinan on 6/3/24.
//

import SwiftUI

struct FlagImage: View {
  let country: String

  var body: some View {
    Image(country)
      .renderingMode(.original)
      .clipShape(RoundedRectangle(cornerSize: .init(width: 10, height: 10)))
      .shadow(radius: 10)
  }
}

#Preview {
  FlagImage(country: "Monaco")
}
