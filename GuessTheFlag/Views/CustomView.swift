//
//  CustomView.swift
//  GuessTheFlag
//
//  Created by Karla Pangilinan on 6/4/24.
//

import SwiftUI

struct RoundedRectText: View {
  let text: String

  var body: some View {
    Text(text)
      .titleStyle()
  }
}

struct CustomView: View {
  var body: some View {
    VStack {
      RoundedRectText(text: "Dexter Morgan")
        .foregroundColor(.red)
      RoundedRectText(text: "Detective Quinn")
      GridStack(rows: 5, columns: 5) { row, col in
        Button("(\(row),\(col))") {
          print("(\(row),\(col))")
        }
      }
      GridStack(rows: 5, columns: 5, content: { row, col in
        Text("(\(row),\(col))")
      })
    }
  }
}


#Preview {
    CustomView()
}
