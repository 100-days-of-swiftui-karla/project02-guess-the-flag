//
//  View+Modifiers.swift
//  GuessTheFlag
//
//  Created by Karla Pangilinan on 6/3/24.
//

import SwiftUI

// MARK: - View Modifiers
struct LargeBlueFont: ViewModifier {
  func body(content: Content) -> some View {
    content
      .font(.largeTitle)
      .foregroundColor(.blue)
  }
}

struct RoundedRect: ViewModifier {
  func body(content: Content) -> some View {
    content
      .font(.largeTitle)
      .foregroundColor(.white)
      .padding()
      .background(.blue)
      .clipShape(Capsule())
  }
}


// MARK: - View extensions
extension View {
  func largeBlueFontStyle() -> some View {
    modifier(LargeBlueFont())
  }
}

extension View {
  func titleStyle() -> some View {
    modifier(RoundedRect()) // no need for `self`
  }
}
