//
//  GuessTheFlagApp.swift
//  GuessTheFlag
//
//  Created by Karla Pangilinan on 5/30/24.
//

import SwiftUI

@main
struct GuessTheFlagApp: App {
  var body: some Scene {
    WindowGroup {
      NavigationStack {
        Form {
          NavigationLink("Guess The Flag") {
            GuessTheFlagView()
          }
          NavigationLink("Alert View") {
            AlertView()
          }
          NavigationLink("Grid Stack & Rounded Rect Text") {
            CustomView()
          }
        }
        .navigationTitle("100 days of SwiftUI: Project 2")
        .navigationBarTitleDisplayMode(.inline)
      }
    }
  }
}
